#Israel Beyene
# exercise 3
Genetic_drift <- function(N,po, genN, rep){
  
  k<-matrix(NA, nrow = genN, ncol = rep)
  k[1,]=po
  for (i in 2:genN){
    k[i,] <- rbinom(rep, N, prob=k[i-1,])/N
  }
  return(k)
}
FST <- function(allelfreq) {
  allelfreq[1,1]=po
  genN = nrow(allelfreq)
  N = ncol(allelfreq)
  k <- matrix(NA, nrow = genN, ncol = 4)
  for (i in 1:genN){
   k[i,1]=mean(allelfreq[i,])
   k[i,2]=var(allelfreq[i,])
   k[i, 3] = k[i, 2]/(po*(1-po))
   k[i, 4] = 1-(1-1/(2*N))^i
  
  }
  k
}

po = 0.5; genN = 1000; rep = 20
drift100 <- Genetic_drift(N=100,po, genN, rep)
j1 <- FST(drift100)
drift500 <- Genetic_drift(N=500,po, genN, rep)
j2 <- FST(drift500)
drift5000 <- Genetic_drift(N=5000,po, genN, rep)
j3 <- FST(drift5000)

a = nrow(j1)
b = nrow(j2)
c = nrow(j3)
#pdf("FST.pdf")

random <- sample(1:1000, 1, replace=F)
randomcolor <- colors()[random]

layout(matrix(c(1:2), nrow = 2))
matplot(drift100, type="l", ylim=c(0,1), xlab="Generation", ylab="Allele Frequency", main=paste("initial freq=0.5 Divergence, N=100"), lty = 1:5)
abline(v=0, h=0.5)
lines(x = seq(1:a), y = j1[,1], type="l", col=(randomcolor), lty = 2, lw = 3)

plot(x = seq(1:a), y = j1[,3], lty = 1, type = "l", col = "blue", ylab = "FST", xlab = "Generations", ylim=c(0,1))
lines(x = seq(1:a), y = j1[,4], lty = "dashed", lw = 2)
layout(c(1,1))

layout(matrix(c(1:2), nrow = 2))
matplot(drift500, type="l", ylim=c(0,1), xlab="Generations", ylab="Allele Frequency", main=paste("initial freq=0.5 Divergence, N=500"), lty = 1:5)
abline(v=0, h=0.5)
lines(x = seq(1:b), y = j2[,1], type="l", col=(randomcolor), lty = 2, lw = 3)

plot(x = seq(1:b), y = j2[,3], lty = 1, type = "l", col = "blue", ylab = "FST", xlab = "Generations", ylim=c(0,1))
lines(x = seq(1:b), y = j2[,4], lty = "dashed", lw = 2)
layout(c(1,1))

layout(matrix(c(1:2), nrow = 2))
matplot(drift5000, type="l", ylim=c(0,1), xlab="Generations", ylab="Allele Frequency", main=paste("initial freq=0.5 Divergence, N=5000"), lty = 1:5)
abline(v=0, h=0.5)
lines(x = seq(1:c), y = j3[,1], type="l", col=(randomcolor), lty = 2, lw = 3)

plot(x = seq(1:c), y = j3[,3], lty = 1, type = "l", col = "blue", ylab = "FST", xlab = "Generations", ylim=c(0,1))
lines(x = seq(1:c), y = j3[,4], lty = "dashed", lw = 2)
layout(c(1,1))

#dev.off()




# There is more diversity in l?arger population than small populaation size. 
#As the lower the population size the highest chance to reach to fixation and higher rate of  drift compared to larger size. 